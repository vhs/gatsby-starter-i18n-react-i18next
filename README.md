# gatsby-starter-i18n-react-i18next

> Gatsby starter for internationalized websites.

Build great-looking multilingual websites and apps with [React](https://reactjs.org/) using [Gatsby](https://www.gatsbyjs.org/).

## Screenshot

![screenshot](<docs/site.example_8080_(iPad).png>)

## Included

- Unit testing framework using [Testing Library](https://testing-library.com/) with integrated [`test-utils`](https://testing-library.com/docs/react-testing-library/setup#configuring-jest-with-test-utils) and some example tests.
- [Chakra UI](https://chakra-ui.com/) component library and design system.
- Functional React component examples written in [TypeScript](https://www.typescriptlang.org/) and using the [React Hooks](https://reactjs.org/docs/hooks-reference.html) API.

## Requirements

- Node
- TypeScript
- Yarn

## Installation

Easy as pie.

1. Copy source code to your machine.
2. Run `yarn` to install dependencies.

## Usage

One command is all you need to get started.

- Run `yarn dev` to start a server for development.
- Run `yarn build` to build site for production.
- Run `yarn serve` to view production site.

Other scripts include:

- Run `yarn test` to run unit tests.
- Run `yarn type-check` to run type checker.

## Rights

Copyright © 2020 VHS <0xc000007b@tutanota.com>

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

This program incorporates work covered by the following copyright and
permission notices:

- Copyright 2010-2018 Adobe (http://www.adobe.com/), with Reserved Font Name 'Source'. All Rights Reserved. Source is a trademark of Adobe in the United States and/or other countries.

- Copyright (c) 2010, Sebastian Kosch (sebastian@aldusleaf.org),
  with Reserved Font Name "Crimson" and "Crimson Text".
