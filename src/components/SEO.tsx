/*!
 * Copyright (C) 2020  VHS <0xc000007b@tutanota.com>
 *
 * This file is part of gatsby-starter-i18n-react-i18next.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React, { FC } from "react";
import { Helmet, HelmetProps } from "react-helmet";
import { useLocation } from "@reach/router";

import { useSiteMetadata, ISiteMetadata } from "../hooks/useSiteMetadata";

type SEOProps = {
  lang?: string;
  title?: string;
  description?: string;
  author?: string;
  image?: string;
  robots?: string;
  keywords?: string[];
  meta?: { name: string; content: string }[];
  links?: { rel: string; href: string }[];
} & HelmetProps;

const SEO: FC<SEOProps> = ({
  title,
  description,
  image,
  author,
  lang,
  robots,
  keywords = [],
  meta = [],
  links = [],
}) => {
  const { pathname } = useLocation();

  const {
    baseUrl,
    lang: defaultLang,
    title: defaultTitle,
    titleTemplate,
    author: defaultAuthor,
    description: defaultDescription,
    image: defaultImage,
    social: { twitterUsername, telegramChannel },
    robots: defaultRobots,
    keywords: defaultKeywords,
  } = useSiteMetadata() as ISiteMetadata;

  const imageUrl = (() => {
    let url = image || defaultImage;
    url = url.replace(/^\/+/, "");
    return url.includes("://") ? url : `${baseUrl}${url}`;
  })();

  const seo = {
    lang: lang || defaultLang,
    title: title || defaultTitle,
    description: description || defaultDescription,
    author: author || defaultAuthor,
    image: imageUrl,
    url: pathname === "/" ? `${baseUrl}` : `${baseUrl}${pathname}`,
    keywords: keywords.length ? keywords : defaultKeywords,
    robots: robots || defaultRobots,
  };

  return (
    <Helmet
      htmlAttributes={{
        lang: seo.lang,
      }}
      title={seo.title}
      titleTemplate={seo.title === defaultTitle ? seo.title : titleTemplate}
      link={[
        {
          rel: "canonical",
          href: seo.url,
        },
      ].concat(links)}
      meta={[
        {
          name: "description",
          content: seo.description,
        },
        {
          name: "author",
          content: seo.author,
        },
        {
          property: "og:title",
          content: seo.title,
        },
        {
          property: "og:description",
          content: seo.description,
        },
        {
          property: "og:image",
          content: seo.image,
        },
        {
          property: "og:type",
          content: "website",
        },
        {
          name: "twitter:card",
          content: "summary_large_image",
        },
        {
          name: "twitter:creator",
          content: twitterUsername,
        },
        {
          name: "twitter:title",
          content: seo.title,
        },
        {
          name: "twitter:description",
          content: seo.description,
        },
        {
          name: "twitter:image",
          content: seo.image,
        },
        {
          name: "telegram:channel",
          content: telegramChannel,
        },
        {
          name: "robots",
          content: seo.robots,
        },
      ]
        .concat(
          seo.keywords.length
            ? {
                name: "keywords",
                content: seo.keywords.join(", "),
              }
            : [],
        )
        .concat(meta)}
    />
  );
};

export default SEO;
