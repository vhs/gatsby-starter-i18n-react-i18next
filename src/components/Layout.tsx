/*!
 * Copyright (C) 2020  VHS <0xc000007b@tutanota.com>
 *
 * This file is part of gatsby-starter-i18n-react-i18next.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import React, { FC } from "react";
import { Box, Stack } from "@chakra-ui/core";

import { useSiteMetadata } from "../hooks/useSiteMetadata";
import NavBar from "./NavBar";
import SEO from "./SEO";

const Layout: FC = ({ children }) => {
  const { title, image } = useSiteMetadata();
  return (
    <>
      <SEO />
      <Box
        as="header"
        _before={{
          content: `""`,
          position: "fixed",
          width: "100%",
          height: "100vh",
          opacity: 0.2,
          backgroundImage: `url(${image})`,
          backgroundSize: "cover",
        }}
      >
        <Box
          bg="transparent"
          color="gray.100"
          p={4}
          style={{ backdropFilter: "brightness(0.7) saturate(80%) blur(5px)" }}
          boxShadow="xl"
        >
          <NavBar siteTitle={title} />
        </Box>
      </Box>
      <Box as="main" p={4}>
        <Stack maxW="960px" margin="0 auto">
          {children}
        </Stack>
      </Box>
    </>
  );
};

export default Layout;
