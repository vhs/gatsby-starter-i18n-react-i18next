module.exports = {
  siteMetadata: {
    baseUrl: "https://site.example/",
    translations: ["id"],
    lang: "en-US",
    title: "Sample Title",
    titleTemplate: "%s | Sample Title",
    description: "Sample website description.",
    author: "info@site.example",
    image: "https://source.unsplash.com/collection/983219/2000x1322",
    social: {
      twitterUsername: "@example",
      instagramUsername: "@example",
      telegramChannel: "@example",
    },
    keywords: ["example", "keywords"],
    robots: "index, follow",
  },
  plugins: ["gatsby-plugin-react-helmet"],
};
